## Summary { data-transition="fade none" }

* Haskell type-checks programs, and does not compile those that fail to type-check
* Haskell infers the types of our programs
* All Haskell functions take exactly one argument
* We can determine how a program behaves by substituting its parts with their definitions
* Haskell programs may be polymorphic in their type

---

## The Haskell Syntax { data-transition="fade none" }

###### Next: More Haskell Syntax, Development Tools and Techniques
