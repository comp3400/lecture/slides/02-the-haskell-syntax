## The Haskell Syntax { data-transition="fade none" }

###### Suppose the following Haskell program

<pre><code class="language-haskell hljs" data-trim data-noescape>
program1 x y = reverse (x ++ "," ++ y)
</code></pre>

* This program has <code>structure</code>
* Specifically, it takes an string (<code>x</code>), then another string (<code>y</code>), and returns a string
* In Haskell, a string is a list of characters, <code>[Char]</code>
* We call this program structure, its <strong>type</strong>

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-inference

<pre><code class="language-haskell hljs" data-trim data-noescape>
program1 x y = reverse (x ++ "," ++ y)
</code></pre>

* Haskell can <em>infer</em> types of expressions
* That is, its type is worked out by the compiler
* If the compiler finds a <em>type-error</em>, it will refuse to compile and report an error

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-inference

<div style="text-align:center">
  <a href="images/type-inference.png">
    <img src="images/type-inference.png" alt="Type Inference" style="width:640px"/>
  </a>
</div>

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-error


<pre><code class="hljs" style="font-size:x-large" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ <mark>'a'</mark> ++ y)
&lt;interactive&gt;:1:37: error:
• Couldn't match expected type ‘[Char]’ with actual type ‘Char’
</code></pre>

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-inference

<pre><code class="language-haskell hljs" data-trim data-noescape>
program1 :: [Char] -> [Char] -> [Char]
program1 x y = reverse (x ++ "," ++ y)
</code></pre>

* We can optionally, supply a type to a Haskell program in our source file
* The double-colon (<code>::</code>) can be pronounced, <q>has the type</q>
* <q>program1 has the type &hellip;</q>
* It is generally accepted good practice to supply this type to our functions

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-inference

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ y)
<mark>Prelude&gt;</mark> :type program1
program1 :: [Char] -&gt; [Char] -&gt; [Char]
<mark>Prelude&gt;</mark> program1 "abc" "def"
"fed,cba"
</code></pre>

* We can optionally, supply a type to a Haskell program in our source file
* The double-colon (<code>::</code>) can be pronounced, <q>has the type</q>
* <q>program1 has the type &hellip;</q>
* It is generally accepted good practice to supply this type to our functions

---

## The Haskell Syntax { data-transition="fade none" }

###### The <code>(->)</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
program1 :: [Char] -&gt; ([Char] -&gt; [Char])
</code></pre>

* The <code>(-&gt;)</code> in a type signature is <strong>right-associative</strong>
* That is, placing redundant parentheses appear right-most

---

## The Haskell Syntax { data-transition="fade none" }

###### Consequently, <em>all functions accept exactly one argument</em>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ y)
<mark>Prelude&gt;</mark> :type program1
program1 :: [Char] -&gt; [Char] -&gt; [Char]
<mark>Prelude&gt;</mark> :type program1 "abc"
[Char] -&gt; [Char]
</code></pre>

* The <code>program1</code> function accepts one argument of the type <code>[Char]</code>
* Its return type is a function of the type <code>[Char] -> [Char]</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### What is the type of <code>program2</code>?

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program2 k = reverse (k "ghi" ++ "jkl")
</code></pre>

* First, what is the type of <code>k</code>?
* What is the return type of <code>program2</code>?

---

## The Haskell Syntax { data-transition="fade none" }

###### What is the type of <code>program2</code>?

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program2 k = reverse (k "ghi" ++ "jkl")
<mark>Prelude&gt;</mark> :type program2
program2 :: ([Char] -> [Char]) -> [Char]
</code></pre>

* <code>k :: [Char] -> [Char]</code> &mdash; a function that takes a string and returns a string
* The return type of <code>program2</code> is <code>[Char]</code>
* Note the left-placed parentheses, which are necessary (i.e. not redundant)

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-checking

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :type program1 "xyz"
program1 "xyz" :: [Char] -> [Char]
<mark>Prelude&gt;</mark> :type program2
program2 :: ([Char] -> [Char]) -> [Char]
</code></pre>

* Haskell will refuse to compile programs where the types do not <em>unify</em>
* Note the type of <code>program1 "xyz"</code> and the argument type of <code>program2</code> are the same &mdash; they unify
* We can pass <code>program1</code> to <code>program2</code> and observe the returned <code>[Char]</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### Type-checking

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ y)
<mark>Prelude&gt;</mark> program2 k = reverse (k "ghi" ++ "jkl")
<mark>Prelude&gt;</mark> program2 (program1 "xyz")
"lkjxyz,ghi"
</code></pre>

* To determine what this program does, substitute the argument <code>k</code> in <code>program2</code> with its given value, <code>program1 "xyz"</code>
* <code>reverse (program1 "xyz" "ghi" ++ "jkl")</code>
* Then apply the same substitution rules to <code>program1</code>
* <code>reverse (reverse ("xyz" ++ "," ++ "ghi") ++ "jkl")</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### This substitution works because we are <em>functional programming</em>

---

## The Haskell Syntax { data-transition="fade none" }

###### Lambda expressions

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program3 x = x
<mark>Prelude&gt;</mark> program3 = &bsol;x -&gt; x
</code></pre>

* These two declarations of <code>program3</code> are <em>equivalent</em>
* The second declaration uses a lambda expression
* The <code>&bsol;</code> is an approximation of the Greek lower-case lambda &lambda;

---

## The Haskell Syntax { data-transition="fade none" }

###### Lambda expressions

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program4 x y = x
<mark>Prelude&gt;</mark> program4 = &bsol;x y -&gt; x
</code></pre>

* Also equivalent, with the second using a lambda expression
* But why would we do this?

---

## The Haskell Syntax { data-transition="fade none" }

###### Lambda expressions

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program5 w = w 99
<mark>Prelude&gt;</mark> double x = x + x
<mark>Prelude&gt;</mark> program5 double
198
</code></pre>

* We have passed in the <code>double</code> function to <code>program5</code> which was applied to <code>99</code>
* Suppose we want to pass a different function e.g. <code>triple</code>
* However, we did not want to explicitly declare the <code>triple</code> function &mdash; just pass it <em>anonymously</em>

---

## The Haskell Syntax { data-transition="fade none" }

###### Lambda expressions

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program5 w = w 99
<mark>Prelude&gt;</mark> program5 (&bsol;x -&gt; x + x + x)
297
</code></pre>

* We have not had to explicitly declare a function to pass to <code>program5</code>
* We declare an anonymous function using a lambda expression
* <q>given <code>x</code>, return <code>x + x + x</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### A point on data types

* So far we have seen the data type <code>Char</code> and the list data which uses <code>[brackets]</code>
* Data types always start with an upper-case character, except for special built-in data types such as lists
* Followed by 0 or more upper-case, lower-case or digit characters

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program6 q = q "Hello"
</code></pre>

* What is the type of <code>program6</code>?
* <code>([Char] -&gt; ?) -&gt; ?</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program6 q = q "Hello"
</code></pre>

* We see that <code>program6</code> takes a function argument (<code>q</code>) and applies it to a string
* But what is the type of <code>q</code>?
* What is the return type of <code>program6</code>?

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program6 q = q "Hello"
</code></pre>

* <code>program6</code> returns <em>anything</em>, as long as that thing is the same type as the return type of <code>q</code>
* We need a way to denote <em>anything</em> and to align those things when necessary

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program6 q = q &quot;Hello&quot;
<mark>Prelude&gt;</mark> :type program6
program6 :: ([Char] -&gt; a) -&gt; a
</code></pre>

* polymorphic types start with a <em>lower-case</em> character
* In this case, the return type of <code>q</code> is the same as the return type of <code>program6</code>, so the same polymorphic type appears (<code>a</code>)
* The <code>program6</code> function can be used with any type in the position of <code>a</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism, another example

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :type \x y -> x
</code></pre>

* What is the type of this lambda expression?
* The type of the argument <code>x</code> and the return type must be the same
* The type of <code>y</code> can be anything and does not relate to <code>x</code>

---

## The Haskell Syntax { data-transition="fade none" }

###### Polymorphism, another example

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> :type \x y -> x
\x y -> x :: a -> b -> a
</code></pre>

---

## The Haskell Syntax { data-transition="fade none" }

###### Function names

* So far, all our function names have started with a lower-case letter
  * <code>program1</code>, <code>program2</code>, etc.
* Function names that start with a lower-case letter are in <em>prefix</em> position, by default
* That is, when we use them, we use the function name, then follow it with an argument list

---

## The Haskell Syntax { data-transition="fade none" }

###### Function names

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ y)
<mark>Prelude&gt;</mark> program1 "abc" "def"
"fed,cba"
<mark>Prelude&gt;</mark> "abc" `program1` "def"
"fed,cba"
</code></pre>

* Function names that start with a lower-case letter can be used in <em>infix</em> position by surrounding it in <code>`backticks`</code>
* That is, the function name appears in-between its argument list

---

## The Haskell Syntax { data-transition="fade none" }

###### Function names

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> program1 x y = reverse (x ++ "," ++ y)
</code></pre>

* We have also seen function names that <em>start with a symbol</em>
* For example, the <code>++</code> function, which appends two lists
* These functions are used in infix position, by default

---

## The Haskell Syntax { data-transition="fade none" }

###### Function names

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>Prelude&gt;</mark> "abc" ++ "def"
"abcdef"
<mark>Prelude&gt;</mark> (++) "abc" "def"
"abcdef"
</code></pre>

* We can use these functions in prefix position by surrounding the function name in <code>parentheses</code>
  
---

