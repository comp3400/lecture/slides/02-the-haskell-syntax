## Revision { data-transition="fade none" }

* Recall
  * Functional Programming is a programming style, with a commitment to preserving <em>referential transparency</em>
  * Haskell is a programming language where this commitment is required
  * Haskell syntax for writing a function e.g. <code>func x y = (x + y) * 2</code>
  * GHC is a compiler for interacting with, and authoring, Haskell programs

---

